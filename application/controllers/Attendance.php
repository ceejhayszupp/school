<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Attendance extends CI_Controller
{
    
	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->library('session');
		
       /*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		
    }
    
    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {

        $page_data['page_name']  = 'attendance';
        $page_data['page_title'] = get_phrase('attendance');
        $this->load->view('attendance/index', $page_data);
    }
    
    public function student($rfid = ''){
        $this->db->select('s.student_id,s.name,s.rfid,s.section_id,se.name as section');
        $this->db->from('student s');
        $this->db->join('section se','se.section_id = s.section_id','left');
        $this->db->where('rfid',$rfid);
        $query = $this->db->get();
        $result = $query->result();

        if(sizeof($result) > 0){
            date_default_timezone_set('Asia/Hong_Kong');
            $date = date('Y-m-d');
            $time = date('Y-m-d H:i:s');
            $student_id = $result[0]->student_id;
            $phql = "INSERT INTO attendance(status,student_id,`date`,time) VALUES (1,'{$student_id}','{$date}','{$time}')
            ON DUPLICATE KEY UPDATE time='{$time}',status=1;";
            $results = $this->db->query($phql);

            echo json_encode(array('result'=> $result[0],'found' =>1,'results'=>$results));    
        }else{
            echo json_encode(array('found' => 0));    
        }
    }
    
}
